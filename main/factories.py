import datetime

import factory
from factory import fuzzy

from main.models import Movie, Comment


class RandomMovieFactory(factory.Factory):
    class Meta:
        model = Movie

    title = factory.Faker('company')
    website = factory.Faker('address')
    rating = fuzzy.FuzzyDecimal(0, 10, 1)
    release_date = fuzzy.FuzzyDate(datetime.date(1950, 1, 1))


class RandomCommentFactory(factory.Factory):
    class Meta:
        model = Comment

    movie = fuzzy.FuzzyChoice(Movie.objects.all())
    content = factory.Faker('company')
