from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet


class PoorViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet
):
    pass
