import math
from datetime import datetime, date

from django.db.models import QuerySet
from django.utils.encoding import force_text
from rest_framework import status
from rest_framework.exceptions import APIException


def convert_date(date_in: str) -> date:
    date_out = datetime.strptime(date_in, '%d %b %Y').date()
    return date_out


def get_top_ten(queryset_in: QuerySet):
    queryset = []
    last_count = math.inf
    last_rating = 0
    rank = 0
    for x in queryset_in:
        if x['total_comments'] < last_count:
            if len(queryset) >= 10:
                break
            rank += 1
            last_count = x['total_comments']
        x['rank'] = rank
        if len(queryset) >= 10 and x['rating'] < last_rating:
            break
        last_rating = x.pop('rating')
        queryset.append(x)
    return queryset


class CustomValidation(APIException):
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    default_detail = 'A server error occurred.'

    def __init__(self, detail, field, status_code, **kwargs):
        if status_code is not None:
            self.status_code = status_code
        if detail is not None:
            self.detail = {field: force_text(detail)}
        else:
            self.detail = {'detail': force_text(self.default_detail)}
        if kwargs:
            self.detail = {**self.detail, **kwargs}
