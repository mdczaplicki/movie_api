from rest_framework import serializers

from main.models import Movie, Comment


class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'title', 'release_date', 'rating', 'website')


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    movie_id = serializers.PrimaryKeyRelatedField(queryset=Movie.objects.all(), source='movie')

    class Meta:
        model = Comment
        fields = ('id', 'movie_id', 'content')
