import requests

from main import utils
from movie_api import settings

_URL = f'http://www.omdbapi.com/?apikey={settings.OMDB_API_KEY}&t=%s'

session = requests.Session()


def get(title):
    response = session.get(_URL % (title if title else ''))
    return response.json()


def get_data(title):
    response = get(title)
    if response['Response'] == 'True':
        date = utils.convert_date(response.get('Released', None))
        data = {
            'title': response.get('Title', None),
            'release_date': date,
            'rating': response.get('imdbRating', None),
            'website': response.get('Website', None)
        }
        return data
    return None
