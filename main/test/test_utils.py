import datetime

from django.db.models import Count, F
from django.test import TestCase

from main.factories import RandomMovieFactory, RandomCommentFactory
from main.models import Movie
from main.utils import get_top_ten, convert_date


class UtilsTest(TestCase):
    def setUp(self) -> None:
        movies = []
        for _ in range(15):
            movie = RandomMovieFactory(rating=5)
            movie.save()
            movies.append(movie)
        RandomMovieFactory(rating=3).save()
        for _ in range(5):
            RandomCommentFactory(movie=movies[0]).save()
        for i in range(10):
            RandomCommentFactory(movie=movies[i + 1]).save()
        self.input_date = '06 Jul 2017'

    def test_get_top_ten_correct(self):
        def flatten(l):
            return {item for sublist in l for item in sublist}

        queryset = Movie.objects.exclude(comments=None) \
            .annotate(total_comments=Count('comments'), movie_id=F('id')) \
            .order_by('-total_comments', '-rating') \
            .values('movie_id', 'total_comments', 'rating')
        output = get_top_ten(queryset)
        keys = flatten(map(lambda x: x.keys(), output))
        self.assertEqual(len(output), 11)
        self.assertEqual(keys, {'movie_id', 'rank', 'total_comments'})

    def test_convert_date(self):
        date = convert_date(self.input_date)
        self.assertEqual(date, datetime.date(2017, 7, 6))
