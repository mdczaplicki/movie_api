import datetime

from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from main.factories import RandomMovieFactory, RandomCommentFactory
from main.test import client


class GetTopTest(TestCase):
    def setUp(self) -> None:
        movies = []
        for _ in range(10):
            movie = RandomMovieFactory(rating=5, release_date=datetime.date(2018, 5, 5))
            movie.save()
            movies.append(movie)
        for _ in range(10):
            movie = RandomMovieFactory(rating=5, release_date=datetime.date(2020, 5, 5))
            movie.save()
            movies.append(movie)
        for _ in range(5):
            RandomCommentFactory(movie=movies[0]).save()
        for i in range(10):
            RandomCommentFactory(movie=movies[-(i+1)]).save()

    def test_top(self):
        response = client.get(reverse('top'))
        self.assertEqual(len(response.data), 11)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_top_date_filter(self):
        response = client.get(reverse('top'), {
            'min_date': '2018-01-01',
            'max_date': '2019-01-01'
        })
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
