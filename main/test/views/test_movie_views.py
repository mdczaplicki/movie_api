from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from main import omdb_client
from main.models import Movie
from main.serializers import MovieSerializer
from main.test import client


class GetSingleMovieTests(TestCase):
    def setUp(self) -> None:
        self.sw = Movie.objects.create(
            title='Star Wars',
            release_date='2010-01-01',
            rating=10.0,
            website='https://starwars.com/'
        )
        self.minions = Movie.objects.create(
            title='Minions',
            release_date='2015-12-12',
            rating=1.0,
            website='https://minions.com/'
        )

    def test_get_valid_single_movie(self):
        response = client.get(reverse('movies-detail', kwargs={'pk': self.sw.pk}))
        sw = Movie.objects.get(pk=self.sw.pk)
        serializer = MovieSerializer(sw)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_movie(self):
        response = client.get(reverse('movies-detail', kwargs={'pk': 100}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PostMoviesTest(TestCase):
    def setUp(self) -> None:
        self.movies = [
            {
                'title': 'Star Wars',
                'release_date': '2010-01-01'
            },
            {
                'title': 'NOT_EXISTING_MOVIE',
                'release_date': '1000-02-02'
            },
            {
                'title': 'Minions'
            },
            {
                'title': 'Indiana Jones',
                'release_date': '2019-01-01',
                'rating': 7.8,
                'website': 'https://indianajones.com/'
            }
        ]

    def test_post_valid_movie(self):
        for movie in self.movies:
            response = client.post(
                reverse('movies-list'),
                data=movie,
                content_type='application/json'
            )
            data = omdb_client.get_data(movie['title'])
            if not data:
                data = movie
            movie = Movie.objects.create(**data)
            serializer = MovieSerializer(movie)
            self.assertEqual(response.data['title'], serializer.data['title'])
            self.assertEqual(response.data['release_date'], serializer.data['release_date'])
            self.assertEqual(response.data['rating'], serializer.data['rating'])
            self.assertEqual(response.data['website'], serializer.data['website'])
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_invalid_movie(self):
        response = client.post(
            reverse('movies-list'),
            data={
                'title': None
            },
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_invalid_movie_2(self):
        response = client.post(
            reverse('movies-list'),
            data={
                'title': ''
            },
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_invalid_movie_3(self):
        response = client.post(
            reverse('movies-list'),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_duplicate(self):
        client.post(
            reverse('movies-list'),
            data={
                'title': 'Star'
            },
            content_type='application/json'
        )
        response = client.post(
            reverse('movies-list'),
            data={
                'title': 'Star'
            },
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)


class GetMultipleMovies(TestCase):
    def setUp(self) -> None:
        self.movie_1 = Movie.objects.create(
            title='Star Wars',
            release_date='2010-01-01',
            rating=10.0,
            website='https://starwars.com/'
        )
        self.movie_2 = Movie.objects.create(
            title='Minions',
            release_date='2015-12-12',
            rating=1.0,
            website='https://minions.com/'
        )
        self.movie_3 = Movie.objects.create(
            title='Indiana Jones',
            release_date=None,
            rating=5.0,
            website=None
        )
        self.movie_2_serializer = MovieSerializer(self.movie_2)
        self.movie_3_serializer = MovieSerializer(self.movie_3)

    def test_get_all_movies(self):
        response = client.get(reverse('movies-list'))
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_sort(self):
        response = client.get(reverse('movies-list'), {
            'sort': 'rating'
        })
        self.assertEqual(response.data[0], self.movie_2_serializer.data)
        self.assertEqual(response.data[1], self.movie_3_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_page(self):
        response = client.get(reverse('movies-list'), {
            'page': 2,
            'page_size': 1,
            'sort': 'rating'
        })
        self.assertEqual(response.data[0], self.movie_3_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_filter(self):
        response = client.get(reverse('movies-list'), {
            'title': 'indiana'
        })
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], self.movie_3_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_multiple_filters(self):
        response = client.get(reverse('movies-list'), {
            'year': '2015',
            'min_date': '2015-12-12',
            'max_date': '2015-12-12',
            'min_rating': 1,
            'max_rating': 1,
            'title': 'minio'
        })
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], self.movie_2_serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_filter(self):
        response = client.get(reverse('movies-list'), {
            'year': '2020'
        })
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
