from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from main.models import Movie, Comment
from main.serializers import CommentSerializer
from main.test import client


class GetCommentsTest(TestCase):
    def setUp(self) -> None:
        self.movie_1 = Movie.objects.create(title='Title of fantastic movie')
        self.movie_2 = Movie.objects.create(title='Very very very very bad movie')
        self.comment_1 = Comment.objects.create(movie=self.movie_1, content='Great movie!')
        self.comment_2 = Comment.objects.create(movie=self.movie_1, content='Fantastic movie!')
        self.comment_3 = Comment.objects.create(movie=self.movie_2, content='Worst movie ever! No recommend!')

    def test_get_all_comments(self):
        response = client.get(reverse('comments-list'))
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_filter(self):
        response = client.get(reverse('comments-list'), {
            'movie_id': self.movie_2.pk
        })
        serializer = CommentSerializer(self.comment_3)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0], serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class PostCommentTest(TestCase):
    def setUp(self) -> None:
        movie = Movie.objects.create(title='It is definitely a movie')
        self.valid_data = {
            'movie_id': movie.pk,
            'content': 'This is a movie, indeed'
        }
        self.invalid_data = {
            'movie_id': '',
            'content': ''
        }

    def test_post_valid_data(self):
        response = client.post(
            reverse('comments-list'),
            data=self.valid_data,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_invalid_data(self):
        response = client.post(
            reverse('comments-list'),
            data=self.invalid_data,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
