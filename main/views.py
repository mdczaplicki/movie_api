from django.db.models import Count, F
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from main import omdb_client
from main import viewsets
from main.filters import MovieFilterSet, CommentFilterSet, TopFilterSet
from main.models import Movie, Comment
from main.serializers import MovieSerializer, CommentSerializer
from main.utils import get_top_ten, CustomValidation


class MovieViewSet(viewsets.PoorViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_class = MovieFilterSet
    ordering_fields = ('release_date', 'title', 'website', 'rating')

    def create(self, request, *args, **kwargs):
        data = omdb_client.get_data(request.data.get('title', ''))
        if not data:
            data = request.data
        existing_movie = Movie.objects.filter(title=data.get('title'))
        if existing_movie.exists():
            raise CustomValidation('Movie already present', 'title',
                                   status.HTTP_409_CONFLICT, movie_id=existing_movie.first().id)
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_paginated_response(self, data):
        return Response(data)


class CommentViewSet(viewsets.PoorViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_class = CommentFilterSet

    def get_paginated_response(self, data):
        return Response(data)


class TopView(ListAPIView):
    filter_class = TopFilterSet

    def get_queryset(self):
        queryset = Movie.objects.exclude(comments=None)\
            .annotate(total_comments=Count('comments'), movie_id=F('id'))\
            .order_by('-total_comments', '-rating')\
            .values('movie_id', 'total_comments', 'rating')
        return queryset

    def list(self, request, *args, **kwargs):
        initial_queryset = self.filter_queryset(self.get_queryset())
        queryset = get_top_ten(initial_queryset)
        return Response(queryset)
