from django_filters import rest_framework as filters

from main.models import Movie, Comment


class MovieFilterSet(filters.FilterSet):
    min_rating = filters.NumberFilter(field_name='rating', lookup_expr='gte')
    max_rating = filters.NumberFilter(field_name='rating', lookup_expr='lte')
    min_date = filters.DateFilter(field_name='release_date', lookup_expr='gte')
    max_date = filters.DateFilter(field_name='release_date', lookup_expr='lte')
    year = filters.CharFilter(field_name='release_date', lookup_expr='year')
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = Movie
        fields = ['release_date', 'min_rating', 'max_rating', 'min_date', 'max_date', 'year', 'title']


class CommentFilterSet(filters.FilterSet):
    movie_id = filters.NumberFilter(field_name='movie__id', lookup_expr='exact')

    class Meta:
        model = Comment
        fields = ['movie_id']


class TopFilterSet(filters.FilterSet):
    min_date = filters.DateFilter(field_name='release_date', lookup_expr='gte')
    max_date = filters.DateFilter(field_name='release_date', lookup_expr='lte')

    class Meta:
        model = Movie
        fields = ['min_date', 'max_date']
