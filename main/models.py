from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=255)
    release_date = models.DateField(null=True)
    rating = models.FloatField(null=True)
    website = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.title


class Comment(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='comments', related_query_name='comments')
    content = models.CharField(max_length=1000)

    def __str__(self):
        return self.content
