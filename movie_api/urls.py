from django.urls import path, include
from rest_framework import routers

from main.views import MovieViewSet, CommentViewSet, TopView

router = routers.DefaultRouter(trailing_slash=False)
router.register('movies', MovieViewSet, basename='movies')
router.register('comments', CommentViewSet, basename='comments')

urlpatterns = [
    path('', include(router.urls)),
    path('top', TopView.as_view(), name='top')
]
