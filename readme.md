# Movie API

## Description
Simple movie REST API that uses OMDb API (https://www.omdbapi.com/) as a reference point.  
API endpoints:

| Endpoint      | Method        |
| ------------- |:-------------:|
| /movies       | POST, GET     |
| /comments     | POST, GET     |
| /top          | GET           |

## 3rd party libraries
* Django REST Framework (https://www.django-rest-framework.org/)  
  Provides a set of tools for building APIs in django
* django-filter (https://github.com/carltongibson/django-filter)  
  Brings filtering boilerplate to the table
* factory_boy (https://github.com/FactoryBoy/factory_boy)  
  Automatizes creation of random movies and comments
* faker (https://github.com/joke2k/faker)  
  Working as a part of factory_boy by generating random data for the models
* SQLite (https://www.sqlite.org/)  
  As the project is not big I've chosen to stay with small library that will hold the data
* docker (https://www.docker.com/)  

## Installation
1. Clone the repository
2. Create file called `secret.py` in movie_api folder with content:
    ```python
    SECRET_KEY = 'your super secret key'
    OMDB_API = 'your omdb api key'
    ```
3. Assuming you have `docker` and `docker-compose` installed, run `docker-compose up` in project root directory
4. API should be running over port 8000

### Notes
* Add your host name to `ALLOWED_HOSTS`
* Set `DEBUG` to `False` if deploying

## Usage
### POST

/movies

* title (mandatory) eg. Star Wars
* release_date (optional (overwritten if title present in OMDb API)) eg. 2015-01-01
* rating (optional (overwritten if title present in OMDb API)) eg. 1.0
* website (optional (overwritten if title present in OMDb API)) eg. https://google.pl/

/comments

* movie_id (mandatory), eg. 1
* content (mandatory) eg. Very good movie!
### GET

/movies

* release_date - show movies released on this day, eg. 2011-01-01
* min_rating - self explanatory
* max_rating - self explanatory
* min_date - self explanatory
* max_date - self explanatory
* year - display movies from specific year, eg. 2011
* title - checked vs icontains in movies titles, eg. wars
* sort - value from list: release_date, title, website, rating; prepend with - to reverse
* page_size - size of page, eg. 2
* page - page number, eg. 2

/comments

* movie_id - filter using movie id
 
/top

* min_date - self explanatory
* max_date - self explanatory